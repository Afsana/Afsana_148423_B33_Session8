public void delete (page page)
{
    try
        {
            deletePageAndAllReferences (page);
        }
    catch (Exception e)
        {
            logError (e);
        }
}
private void deletePageAndAllReferences (Page Page) throws Exception
{
    deletePage (page);
    registry.deleteReference (page.name);
    configKeys.deleteKey (page.name.makeKey());
}
private void logError (Exception e)
{
    logger.log (e.getMessage());
}